import flask

server = flask.Flask(__name__)

@server.route("/")
def hello():
    return "hello"
if __name__ == "__main__":
    server.run(host='0.0.0.0')
